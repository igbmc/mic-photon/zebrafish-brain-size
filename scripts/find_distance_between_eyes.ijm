// ******************** Requirements ******************
// https://github.com/biovoxxel/bv3dbox#installation

// Main 
// Select folder with data
dirName = getDirectory("Choose Directory with images");

// Get list of files in folder
fileList = getFileList(dirName);

// Select folder for output
dirOutputName = getDirectory("Choose Directory for Output");
// Loop through images
for (i=0; i<fileList.length; i++) {
	// Only process it if it's a tif image
	if (endsWith(fileList[i], ".tif")) {
		// Open it
		open(dirName + fileList[i]);
		// Get image name for reference
		img_name = getTitle();
		// Find eyes
		FindEyes(img_name, dirOutputName);
		// Find line between eyes
		FindLineBetweenEyes(img_name, dirOutputName);
	}
}

// ***************** Functions ***********************
function FindEyes(original_image, dirOutputName) { 
// adapted from https://forum.image.sc/t/segment-large-black-blobs-zebrafish-eyes-in-brightfield-images/77015/2
	roiManager("reset");
	resetMinAndMax();
	setOption("ScaleConversions", true);
	selectWindow(original_image);
	run("8-bit");
	run("Invert");	//if eyes are dark and background bright
	run("Scale...", "x=0.25 y=0.25 interpolation=None average create");
	downscaled = getTitle();
	run("Voronoi Threshold Labler (2D/3D)", "filtermethod=Open filterradius=36.0 backgroundsubtractionmethod=DoG backgroundradius=100.0 histogramusage=full thresholdmethod=Moments fillholes=Off separationmethod=[Maxima Spheres] spotsigma=20.0 maximaradius=20.0 volumerange=0-Infinity excludeonedges=true outputtype=Labels stackslice=1 applyoncompleteimage=false processonthefly=true");
	segmented = getTitle();
	run("Scale...", "x=4 y=4 interpolation=None average create");
	upscaled = getTitle();
	run("Median...", "radius=5");	//just smoothens the outlines
	
	setAutoThreshold("Default dark");
	setThreshold(1, 1000);
	run("Convert to Mask");
	run("Analyze Particles...", "size=100000-300000 circularity=0.50-1.00 add");
	n = roiManager("count");
	if (n != 2){
		selectWindow("ROI Manager");
		run("Close");
		
		run("Watershed");		
		run("Analyze Particles...", "size=100000-300000 circularity=0.76-1.00 add");
	}

	close(downscaled);
	close(upscaled);
	close(segmented);
	selectWindow(original_image);
	run("Revert");
	roiManager("Show All");
	// Save detected ROIs
	roiManager("save", dirOutputName + original_image+"_eyes_rois.zip");
}

function FindLineBetweenEyes(original_image, dirOutputName) { 
// adapted from https://github.com/ved-sharma/Shortest_distance_between_objects/blob/f444d3713505478254fc0fdf5471539af0b22664/data/Shortest_distance%20between%202D%20objects_v03.ijm
// Author: Ved P. Sharma, 	January 24, 2018

	selectWindow(original_image);
	width = getWidth();
	height = getHeight();
	if(roiManager("count") < 2)
		exit("The macro needs atleast 2 area ROIs in the ROI Manager");
	
	strokeWd = 2;
	strokeCol = "cyan";
	interval = 2 ;
	exclude = false;
	
	roiManager("select", 0);
	if(selectionType() == 9)
		roiManager("Split");
	else
		roiManager("Add"); // if ROI1 is not a composite
	n = roiManager("count") - 2; // no. of individual ROI in composite ROI #1
	
	roiManager("select", 1);
	if(selectionType() == 9)
		roiManager("Split");
	else
		roiManager("Add"); // if ROI2 is not a composite
	m = roiManager("count") - n - 2; // no. of individual ROI in composite ROI #2
	
	// concat x and y values of all the ROIs in ROI #2
	roiManager("select", 2+n);
	run("Interpolate", "interval=&interval adjust");
	roiManager("update");
	getSelectionCoordinates(u, v);
	for(i=1; i<m; i++) {
		roiManager("select", 2+n+i);
		run("Interpolate", "interval=&interval adjust");
		roiManager("update");
		getSelectionCoordinates(ui, vi);
		u = Array.concat(u, ui);
		v = Array.concat(v, vi);
	}
	len2 = u.length;
	
	count1 = 0; // count of 2D ROIs close to the image edges
	count2 = 0; // count of 2D ROIs in set 1 which intersect ROIs in set 2
	// this for loop runs on each ROI in composite ROI #1
	for(r=0; r<n; r++) {
		roiManager("select", 2+r);
		run("Interpolate", "interval=&interval adjust");
		roiManager("update");
		getSelectionCoordinates(x, y);
		len1 = x.length;
	
		if(ROIintersecting()) {// checking to see if the current area ROI (x,y) from set 1 intersects any of the ROIs in set 2
			count2++;
	//		print("ROI "+(r+1)+" from set 1 is intersecting set 2 ROI");
		}
		else {
			distFinal = sqrt((u[0]-x[0])*(u[0]-x[0]) + (v[0]-y[0])*(v[0]-y[0]));
			xStart = x[0];
			yStart = y[0];
			uEnd = u[0];
			vEnd = v[0];
			for(j=1; j<len2; j++)
				for(i=1; i<len1; i++) {
				dist = sqrt((u[j]-x[i])*(u[j]-x[i]) + (v[j]-y[i])*(v[j]-y[i]));
				if(dist < distFinal) {
					distFinal = dist;
					xStart = x[i];
					yStart = y[i];
					uEnd = u[j];
					vEnd = v[j];
				}
			}
			if(exclude) {
				if(closeToBoundary(r))
					count1++;
				else {
					makeLine(xStart, yStart, uEnd, vEnd);
					Roi.setStrokeColor(strokeCol);
					Roi.setStrokeWidth(strokeWd);
					roiManager("add");
					roiManager("Select", roiManager("count")-1);
					roiManager("Rename", "line "+(r+1));
				}
			}
			else {
				makeLine(xStart, yStart, uEnd, vEnd);
				Roi.setStrokeColor(strokeCol);
				Roi.setStrokeWidth(strokeWd);
				roiManager("add");
				roiManager("Select", roiManager("count")-1);
				roiManager("Rename", "line "+(r+1));
			}
		}
	}
	// delete split ROIs at the end
	for(i=0; i<n+m; i++) {
		roiManager("select", 2);
		roiManager("Delete");
	}
	
	// Select the 3 ROI --> which is the line
	roiManager("Select", 2);
	// Measure it
	roiManager("Measure");
	// Save it to csv
	saveAs("Results", dirOutputName + original_image + "_eyes_results.csv");
	// Create overal with annotations
	run("Flatten");
	// Save overlay to disk
	save(dirOutputName + original_image);
	close("ROI Manager");
	close("*");
	if (isOpen("Results")) {
		selectWindow("Results");
		run("Close"); 
		}
}

function ROIintersecting() {
	for(i=2+n; i<2+n+m; i++) { // i local variable; n, m, x, y, len1 global
		roiManager("select", i);
		for(j=0; j<len1; j++)
			if(Roi.contains(x[j], y[j]))
				return true;
	}
	return false;
}

function closeToBoundary (a) { // a local variable, is the number of the ROI in set 1, to be checked for being close to the boundary
	roiManager("select", 2+a);
	getSelectionBounds(rect_x0, rect_y0, rectWd, rectHt);
	if(rect_x0 < distFinal)
		return true;
	else if ((width - rect_x0 - rectWd) < distFinal)
		return true;
	else if (rect_y0 < distFinal)
		return true;
	else if ((height - rect_y0 - rectHt) < distFinal)
		return true;
	else
		return false;
}