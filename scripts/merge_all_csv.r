path = "./../Output/"

file_list <- list.files(path, pattern = 'eyes_results.csv', full.names = TRUE)
#read the files in as plaintext
csv_list <- lapply(file_list , readLines)

#remove the header from all but the first file
csv_list[-1] <- sapply(csv_list[-1], "[", 2)

#unlist to create a character vector
csv_list <- unlist(csv_list)

#write the csv as one single file
writeLines(text=csv_list,con=file.path(path, "eyes_distances.csv"))

#read the csv as one single file
df <- read.csv(file.path(path, "eyes_distances.csv"))

# Show histogram of eyes distance for fun
# hist(df$Length)
